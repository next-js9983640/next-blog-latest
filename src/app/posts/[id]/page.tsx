import React from 'react'

function Post({ params }: { params: { id: string } }) {
    return (
        <div>this is a Post with id {params.id}</div>
    )
}

export default Post